package org.annotate.ontology;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.xml.sax.*;

import edu.stanford.bmir.protege.examples.view.ExampleViewComponent;

import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLEntity;
import org.w3c.dom.*;
import org.apache.log4j.Logger;

public class AxiomAnalyser {
	
	String theAxiom;
	private Document doc;
	private static final Logger log = Logger.getLogger(ExampleViewComponent.class);
	
	public AxiomAnalyser(String axiom,String xml) throws Exception{
		this.theAxiom=axiom;
		doc=Test.getDocument();
	}
	
	public void setDocument(Document doc){
		this.doc=doc;
	}
	
	public Document getDocument(){
		return doc;
	}
	
	public Element analyse(String subject,String axiom) throws Exception{
		Element e = null;
		
		String axiomType= axiom.substring(0,axiom.indexOf("(") );
		//system.out.println("axiom type: "+axiomType);
		String innerAxiom = axiom.substring(axiom.indexOf("(")+1,axiom.lastIndexOf(")"));
		//system.out.println("inner axiom: "+innerAxiom);
		
		if (axiomType.equals("SubClassOf")){
			subClassOf(innerAxiom,"SubClassOf");
		}else if (axiomType.equals("EquivalentClasses")){
			subClassOf(innerAxiom,"EquivalentClasses");
		}else if (axiomType.equals("ObjectSomeValuesFrom")){
			e = objectSomeValuesFrom(subject,innerAxiom,"ObjectSomeValuesFrom");
		}else if (axiomType.equals("ObjectAllValuesFrom")){
			e = objectSomeValuesFrom(subject,innerAxiom,"ObjectAllValuesFrom");
		}else if (axiomType.equals("ObjectIntersectionOf")){
			e = objectIntersectionOf(subject,innerAxiom,"ObjectIntersectionOf");
		}else if (axiomType.equals("ObjectUnionOf")){
			e = objectIntersectionOf(subject,innerAxiom,"ObjectUnionOf");
		}else if (axiomType.equals("ObjectComplementOf")){
			e = objectIntersectionOf(subject,innerAxiom,"ObjectComplementOf");
		}else if (axiomType.equals("DisjointClasses")){
			e = objectIntersectionOf(subject,innerAxiom,"DisjointClasses");
		}else{
			axiomType = axiomType.replaceAll("[^A-Za-z0-9]", "");
			e=doc.createElement(axiomType);
			e.setAttribute("info", "To be added functionality");
		}
		
		return e;
	}
	
	public Element analyse(String subject,String axiomType,String innerAxiom) throws Exception{
		Element e = null;
		
		if (axiomType.equals("SubClassOf")){
			subClassOf(innerAxiom,"SubClassOf");
		}else if (axiomType.equals("EquivalentClasses")){
			subClassOf(innerAxiom,"EquivalentClasses");
		}else if (axiomType.equals("ObjectSomeValuesFrom")){
			e = objectSomeValuesFrom(subject,innerAxiom,"ObjectSomeValuesFrom");
		}else if (axiomType.equals("ObjectAllValuesFrom")){
			e = objectSomeValuesFrom(subject,innerAxiom,"ObjectAllValuesFrom");
		}else if (axiomType.equals("ObjectIntersectionOf")){
			e = objectIntersectionOf(subject,innerAxiom,"ObjectIntersectionOf");
		}else if (axiomType.equals("ObjectUnionOf")){
			e = objectIntersectionOf(subject,innerAxiom,"ObjectUnionOf");
		}else if (axiomType.equals("ObjectComplementOf")){
			e = objectIntersectionOf(subject,innerAxiom,"ObjectComplementOf");
		}else if (axiomType.equals("DisjointClasses")){
			e = objectIntersectionOf(subject,innerAxiom,"DisjointClasses");
		}else{
			axiomType = axiomType.replaceAll("[^A-Za-z0-9]", "");
			e=doc.createElement(axiomType);
			e.setAttribute("info", "To be added functionality");
		}
		
		return e;
	}

	public void subClassOf(String values,String tag) throws Exception{
		
		Element e=null;
		
		String subject = values.substring(0,values.indexOf(" "));
		//system.out.println("subject: "+subject);
		String subjectClass = subject.substring(subject.indexOf("#")+1,subject.length()-1);
		String subjectIRI = subject.substring(1,subject.length()-1);
		log.info("subjectIRI: "+subjectIRI);
		
		String axiom=values.substring(values.indexOf(" ")+1,values.length());
		
		Node subjectEntityNode = elementExists(subjectClass,doc,"Entity");
		if ( subjectEntityNode==null) {
            Entity subjectEntity = new Entity(subjectIRI);
            doc = subjectEntity.createEntity(doc);
            
            e=subjectEntity.getE();
            //String pattern = values.replaceAll("[^A-Za-z0-9]", "");
            //e.setAttribute("pattern", pattern);
    		//printDocument();
        }else{
        	log.info("Entity not null! "+subjectClass);
        	e=(Element)subjectEntityNode;
        }
		
		String axiomType="";
		if (axiom.charAt(0)!='<'){
			axiomType= axiom.substring(0,axiom.indexOf("(") );
			
			log.info("axiom type: "+axiomType+" char 0: "+axiom.charAt(0));
			String innerAxiom = axiom.substring(axiom.indexOf("(")+1,axiom.lastIndexOf(")"));
			log.info("inner axiom: "+innerAxiom);
			
			String subjectId = e.getAttributes().getNamedItem("id").getNodeValue();
			
			Element subElement=analyse(subjectId,axiomType,innerAxiom);
			
			//set the reference for that element
			e.appendChild(subElement);
			
		}else{
			subjectClass = axiom.substring(axiom.indexOf("#")+1,axiom.length()-1);
			subjectIRI = axiom.substring(1,axiom.length()-1);
			
			Node objectEntityNode = elementExists(subjectClass,doc,"Entity");
			String id="";
			
			if ( objectEntityNode==null) {
	            Entity subjectEntity = new Entity(subjectIRI);
	            doc = subjectEntity.createEntity(doc);
	            id=subjectEntity.getId();
	            
	        }else{
	        	log.info("Entity not null! "+subjectClass);
	        	e=(Element)subjectEntityNode;
	        	id=objectEntityNode.getAttributes().getNamedItem("id").getNodeValue();
	        }
		        
	        Element subElement=doc.createElement(tag);
	        subElement.setAttribute("name", subjectClass);
	        subElement.setAttribute("ref", id);
	        //Test.incElementId();
	        
	        if (e!=null)
	        	e.appendChild(subElement);
		}
		
	}
	
	public Element objectSomeValuesFrom(String subject,String values,String type) throws Exception{
		
		Node subjectElement = elementExists(subject,doc,"Entity");
		String subjectName="?";
		
		Element e=doc.createElement(type);
		String pattern = values.replaceAll("[^A-Za-z0-9]", "");
        e.setAttribute("pattern", pattern);
		
		String relation = values.substring(0,values.indexOf(" "));
		log.info("subject: "+relation);
		String relationClass = relation.substring(relation.indexOf("#")+1,relation.length()-1);
		String relationIRI = relation.substring(1,relation.length()-1);
		log.info("subjectClass: "+relationClass);
		
		String axiom=values.substring(values.indexOf(" ")+1,values.length());
		
		Node relationship = elementExists(relationClass,doc,"Relationship");
		/* prepare the words and pattern values here */
		if ( relationship == null) {
            Relationship rel = new Relationship(relationIRI,type);
            rel.createEntity(doc);
            
            relationship=rel.getE();
            
            e.setAttribute("ref", rel.getId());
        	e.setAttribute("refName", rel.getName());
        }else{
        	String id = relationship.getAttributes().getNamedItem("id").getNodeValue();
        	String name = relationship.getAttributes().getNamedItem("name").getNodeValue();
        	
        	log.info("Entity not null: "+relationClass+" Id: "+id);
        	
        	e.setAttribute("ref", id);
        	//e.setAttribute("refName", name);
        }
		
		/* subject name*/
		if (subjectElement!=null){
			subjectName=subjectElement.getAttributes().getNamedItem("name").getNodeValue();
		}
		
		/* create element to give to upper class */
		String r1Id,r2Id;
		Node r1Node,r2Node;
		String axiomType="";
		
		/* Get roles from relationship */
		NodeList nl = ((Element)relationship).getElementsByTagName("Role");
		log.info("nodes are: "+nl.getLength());
		printElement(relationship);
		if (nl.item(0).getAttributes().getNamedItem("name").getNodeValue().equals("r1")){
			r1Id=nl.item(0).getAttributes().getNamedItem("id").getNodeValue();
			r2Id=nl.item(1).getAttributes().getNamedItem("id").getNodeValue();
			r1Node=nl.item(0);
			r2Node=nl.item(1);
		}else{
			r1Id=nl.item(1).getAttributes().getNamedItem("id").getNodeValue();
			r2Id=nl.item(0).getAttributes().getNamedItem("id").getNodeValue();
			r1Node=nl.item(1);
			r2Node=nl.item(0);
		}
		
		if (axiom.charAt(0)!='<'){
			axiomType= axiom.substring(0,axiom.indexOf("(") );				
			
			log.info("axiom type: "+axiomType);
			String innerAxiom = axiom.substring(axiom.indexOf("(")+1,axiom.lastIndexOf(")"));
			log.info("inner axiom: "+innerAxiom);
			
			Element r1=doc.createElement("RoleRef");
	        r1.setAttribute("ref", r1Id);
	        r1.setAttribute("actorName", subjectName);
	        r1.setAttribute("name", "r1");
	        
	        Element r2=doc.createElement("RoleRef");
			Element rElement=analyse("?",axiomType,innerAxiom);
			r2.setAttribute("ref", r2Id);
		    r2.setAttribute("name", "r2");
		    r2.appendChild(rElement);
		        
		    e.appendChild(r1);
		    e.appendChild(r2);
			
		}else{
			String objectClass = axiom.substring(axiom.indexOf("#")+1,axiom.length()-1);
			//system.out.println("subjectClass2: "+subjectClass);
			String subjectIRI = axiom.substring(1,axiom.length()-1);
			//create the roles
	        Element r1=doc.createElement("RoleRef");
	        r1.setAttribute("ref", r1Id);
	        r1.setAttribute("actorName", subjectName);
	        r1.setAttribute("name", "r1");
	        
	        Element r2=doc.createElement("RoleRef");
	        r2.setAttribute("ref", r2Id);
	        r2.setAttribute("name", "r2");
	        
	        Element objectElement=null;
			Node objectEntityNode = elementExists(objectClass,doc,"Entity");
			
			if ( objectEntityNode==null) {
	            Entity objectEntity = new Entity(subjectIRI);
	            doc = objectEntity.createEntity(doc);
	            
	            r2.setAttribute("actorName", objectEntity.getName());
	            
	            objectElement=objectEntity.getE();
	    		//printDocument();
	        }else{
	        	String id = objectEntityNode.getAttributes().getNamedItem("id").getNodeValue();
	        	String name = objectEntityNode.getAttributes().getNamedItem("name").getNodeValue();
	        	
	        	log.info("Entity not null: "+objectClass+" Id: "+id);
	        	
	        	r2.setAttribute("ref", r2Id);
	        	r2.setAttribute("actorName", name);
	        	
	        	objectElement=(Element)objectEntityNode;
	        }

	        e.appendChild(r1);
	        e.appendChild(r2);
	        
	        Element e2=(Element)e.cloneNode(true);
	        log.info("%%%%%% objEle: "+objectElement.getAttributes().getNamedItem("name").getNodeValue());
	        
	        objectElement.appendChild(e2);
			
		}
		
		return e;
	}
	
	public String checkAxioms(String subject,Element e,String values) throws Exception{
		
		String axiom2="";
		
		int bracketCount = 0;
		
		loop:
		for (int i=0;i<values.length();i++){
			if (values.charAt(i)=='(')
				bracketCount++;
			else if (values.charAt(i)==')'){
				bracketCount--;
				
				if (bracketCount ==0){
					String axiom1="";
					if (values.length()>=i){
						axiom1 = values.substring(0,i+1);
						log.info("**axiom: "+axiom1);
					}else{
						axiom1 = values.substring(0,i);
						log.info("##axiom: "+axiom1);
					}
					
					Element subElement=analyse(subject,axiom1);
					e.appendChild(subElement);
					
					axiom2 = values.replace(axiom1, "");
					axiom2 = axiom2.replaceAll("^\\s+", "");
					
					//system.out.println("Replaced axiom: "+axiom2);
					
					break loop;
				}
			}
		}
		
		return axiom2;
	}
	
	public Element objectIntersectionOf(String subject,String values,String tag) throws Exception{
		
		Element e=doc.createElement(tag);
		String pattern = values.replaceAll("[^A-Za-z0-9]", "");
        e.setAttribute("pattern", pattern);
		
		//check if the intersections are classes or axioms
		char check = values.charAt(0);
		log.info("values: "+values);
		log.info("check char: "+check);
		
		/* Create the reference types for these if not available */
		Node node = elementExists(tag,doc,"Constructor");
		String refId="";
		/* prepare the words and pattern values here */
		if ( node == null) {
            if (tag.equals("ObjectIntersectionOf")){
            	Intersection intersection = new Intersection(tag);
            	intersection.createEntity(doc);
            	refId=intersection.getId();
            }else if (tag.equals("ObjectUnionOf")){
            	Union union = new Union(tag);
            	union.createEntity(doc);
            	refId=union.getId();
            }else if (tag.equals("ObjectComplementOf")){
            	Complement complement = new Complement(tag);
            	complement.createEntity(doc);
            	refId=complement.getId();
            }
        }else{
        	refId=node.getAttributes().getNamedItem("id").getNodeValue();
        }
		
		e.setAttribute("ref", refId);
		
		if (check == '<'){
			String entity = values;
			
			try{
				entity = values.substring(0,values.indexOf(" "));
			}catch (Exception exc){
				
			}
			
			log.info("subject: "+subject);
			String subjectClass = entity.substring(entity.indexOf("#")+1,entity.length()-1);
			String subjectIRI = entity.substring(1,entity.length()-1);
			log.info("subjectClass: "+subjectClass);
			
			Node subjectEntityNode = elementExists(subjectClass,doc,"Entity");
			Element entityRef=doc.createElement("EntityRef");
			
			if ( subjectEntityNode==null) {
	            Entity subjectEntity = new Entity(subjectIRI);
	            doc = subjectEntity.createEntity(doc);
	            
	            entityRef.setAttribute("ref", subjectEntity.getId());
	            entityRef.setAttribute("refName", subjectEntity.getName());
	            e.appendChild(entityRef);
	    		//printDocument();
	        }else{
	        	String id = subjectEntityNode.getAttributes().getNamedItem("id").getNodeValue();
	        	String name = subjectEntityNode.getAttributes().getNamedItem("name").getNodeValue();
	        	
	        	log.info("Entity not null: "+subjectClass+" Id: "+id);
	        	
	        	entityRef.setAttribute("ref", id);
	        	entityRef.setAttribute("refName", name);
	            e.appendChild(entityRef);
	        }
			
			String axiom=null;
			
			if (values.indexOf(" ")>=0)
				axiom=values.substring(values.indexOf(" ")+1,values.length());
			
			String axiomType="";
			
			if (axiom != null && axiom.charAt(0)!='<'){
				axiomType= axiom.substring(0,axiom.indexOf("(") );
				
				log.info("axiom: "+axiom);
				String innerAxiom = axiom.substring(axiom.indexOf("(")+1,axiom.lastIndexOf(")"));
				log.info("inner axiom: "+innerAxiom);
				
				Element element = analyse(subject,axiomType,innerAxiom);
				e.appendChild(element);
				
			}else if (axiom!=null){ //Then its a class not an inner axiom
				log.info("axiom?: "+axiom);
				
				while (!axiom.equals("") && axiom.charAt(0)=='<'){
					
					log.info("axiom b4! "+axiom);
					if(axiom.contains(" ")){
						subjectClass = axiom.substring(axiom.indexOf("#")+1,axiom.indexOf(" ")-1);
						
						if (axiom.contains(" "))
							subjectIRI = axiom.substring(1,axiom.indexOf(" ")-1);
						else
							subjectIRI = axiom.substring(1,axiom.length()-1);
						
						axiom=axiom.substring(axiom.indexOf(" ")+1,axiom.length());
						
						log.info("union iri: "+subjectIRI);
						log.info("axiom after! "+axiom);
						
					}else{
						subjectClass = axiom.substring(axiom.indexOf("#")+1,axiom.length()-1);
						subjectIRI = axiom.substring(1,axiom.length()-1);
						entityRef=doc.createElement("EntityRef");
						axiom="";
					}
					
					subjectEntityNode = elementExists(subjectClass,doc,"Entity");
					if ( subjectEntityNode==null) {
			            Entity subjectEntity = new Entity(subjectIRI);
			            doc = subjectEntity.createEntity(doc);
			            
			            entityRef.setAttribute("ref", subjectEntity.getId());
			            e.appendChild(entityRef);
			    		//printDocument();
			        }else{
			        	String id = subjectEntityNode.getAttributes().getNamedItem("id").getNodeValue();
			        	log.info("Entity not null: "+subjectClass+" Id: "+id);
			        	
			        	entityRef.setAttribute("ref", id);
			            e.appendChild(entityRef);
			        }
					
				}
				
				if (axiom != ""){
					e.appendChild(analyse(subject,axiom));
				}
				
			}
			
		}else{
			//first axiom
			String axiom=checkAxioms(subject,e,values);
			log.info("******* First round axiom: "+axiom);
			
			//loop:
			while (!(axiom=checkAxioms(subject,e,axiom)).equals("")){

				if (axiom.charAt(0)=='<'){
					
					String subjectClass = axiom.substring(axiom.indexOf("#")+1,axiom.length()-1);
					String subjectIRI = axiom.substring(1,axiom.length()-1);
					log.info("subjectClass2: "+subjectClass);
					
					Node subjectEntityNode = elementExists(subjectClass,doc,"Entity");
					Element entityRef=doc.createElement("EntityRef");
					
					if ( subjectEntityNode==null) {
			            Entity subjectEntity = new Entity(subjectIRI);
			            doc = subjectEntity.createEntity(doc);
			            
			            entityRef.setAttribute("ref", subjectEntity.getId());
			            e.appendChild(entityRef);
			    		//printDocument();
			        }else{
			        	String id = subjectEntityNode.getAttributes().getNamedItem("id").getNodeValue();
			        	log.info("Entity not null: "+subjectClass+" Id: "+id);
			        	
			        	entityRef.setAttribute("ref", id);
			            e.appendChild(entityRef);
			        }
					
				}
				//break loop;
			}
			
		}
		
		return e;
	}
	
	public void addEntity(OWLEntity cls) throws Exception{
		
		String iri=cls.getIRI().toString();
		String subjectClass=iri.substring(iri.indexOf("#")+1,iri.length());
		log.info("iri: "+iri+" subjectclass: "+subjectClass);
		
		if (cls instanceof OWLClass){
			
			Node subjectEntityNode = elementExists(subjectClass,doc,"Entity");
			if ( subjectEntityNode==null) {
	            Entity subjectEntity = new Entity(iri);
	            doc = subjectEntity.createEntity(doc);
			}
		}else{
			Node subjectEntityNode = elementExists(subjectClass,doc,"Relationship");
			if ( subjectEntityNode==null) {
				Relationship rel = new Relationship(iri,"");
		        rel.createEntity(doc);
			}
			
		}
	}
	
	public void deleteEntity(String iri) throws Exception{
		//String iri=IRI.toString();
		String subjectClass=iri.substring(iri.indexOf("#")+1,iri.length());
		log.info("iri: "+iri+" subjectclass: "+subjectClass);
		
		Node subjectEntityNode = elementExists(subjectClass,doc,"Entity");
		Node subjectRelNode = elementExists(subjectClass,doc,"Relationship");
		
		if ( subjectEntityNode != null) {
			log.info("in axiom analysis del Entity");
			Element rootElement = doc.getDocumentElement();
            rootElement.removeChild(subjectEntityNode);
		}else if (subjectRelNode != null){
			log.info("in axiom analysis del Relationship");
			Element rootElement = doc.getDocumentElement();
            rootElement.removeChild(subjectRelNode);
		}else{
			log.info("Could not find the class to delete !! "+subjectClass);
		}
	}
	
	public Element deleteAxiom(Element e,String axiom) throws Exception{
		
		String innerAxiom = axiom.substring(axiom.indexOf("(")+1,axiom.lastIndexOf(")"));
		innerAxiom = innerAxiom.substring(innerAxiom.indexOf("(")+1,innerAxiom.lastIndexOf(")"));
		
		String pattern = innerAxiom.replaceAll("[^A-Za-z0-9]", "");
		
		log.info("looking for:\n"+pattern);
		NodeList nl=e.getChildNodes();
		
		if (nl.getLength() > 0) {
			log.info("length is: "+nl.getLength());
	    	for (int i=0;i<nl.getLength();i++){
	    		printElement(nl.item(i));
	    		if (hasAttributeName(nl.item(i),"pattern")){
	    			String nodePattern = nl.item(i).getAttributes().
	    					getNamedItem("pattern").getNodeValue();
	    			log.info(" ^^^^ pattern ^^^^\n"+nodePattern);
	    			if (nodePattern.equals(pattern)){
	    				log.info(" %%%% child to delete found %%%%");
	    				e.removeChild(nl.item(i));
	    			}
	    		}
	    	}
	    }
		
		return e;
	}
	
	public Node elementExists(String name, Document doc, String tag) {
	    boolean exists=false;
	    Node node=null;
	    NodeList nl;
	    nl = doc.getElementsByTagName(tag);
	    log.info("number of "+tag+": "+nl.getLength());
	    if (nl.getLength() > 0) {
	    	for (int i=0;i<nl.getLength();i++){
	    		if (hasAttributeValue(nl.item(i),name)){
	    			node=nl.item(i);
	    			exists=true;
	    		}
	    	}
	    }
	    return node;
	}
	
	public static boolean hasAttributeValue(Node element, String value) {
	    NamedNodeMap attributes = element.getAttributes();
	    for (int i = 0; i < attributes.getLength(); i++) {
	        Node node = attributes.item(i);
	        //log.info("att value : "+node.getNodeValue());
	        if (value.equals(node.getNodeValue())) {
	            return true;
	        }
	    }
	    return false;
	}
	
	public static boolean hasAttributeName(Node element, String value) {
		
	    NamedNodeMap attributes = element.getAttributes();
	    
	    try{
		    for (int i = 0; i < attributes.getLength(); i++) {
		        Node node = attributes.item(i);
		        if (value.equals(node.getNodeName())) {
		            return true;
		        }
		    }
	    }catch(Exception ex){
	    	
	    }
	    
	    return false;
	}
	
	public void printElement(Node node) throws Exception{
		TransformerFactory transFactory = TransformerFactory.newInstance();
		Transformer transformer = transFactory.newTransformer();
		StringWriter buffer = new StringWriter();
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		transformer.transform(new DOMSource(node),
		      new StreamResult(buffer));
		String str = buffer.toString();
		log.info("Element : "+str);
	}
	
	public void printDocument() throws Exception{
		// write the content into xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
		
		DOMSource source = new DOMSource(doc);
		// Output to console for testing
		StreamResult result = new StreamResult(new StringWriter());
		transformer.transform(source, result);
		
		String xmlString = result.getWriter().toString();
		log.info(xmlString);
	}
	
	public void printElement(Element e) throws Exception{
		// write the content into xml file
		TransformerFactory transFactory = TransformerFactory.newInstance();
		Transformer transformer = transFactory.newTransformer();
		StringWriter buffer = new StringWriter();
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		transformer.transform(new DOMSource(e),
		      new StreamResult(buffer));
		String str = buffer.toString();
		log.info(str);
	}

}
