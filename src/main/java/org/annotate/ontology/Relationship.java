package org.annotate.ontology;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.xml.sax.*;
import org.w3c.dom.*;

public class Relationship {
	
	private String iri;
	private String name;
	private String type;
	private String nAry="2";
	private String id;
	private Element e;
	
	public Relationship(String iri,String type){
		this.type=type;
		this.iri=iri;
		this.name=iri.substring(iri.indexOf("#")+1,iri.length());
		System.out.println("Entity name: "+name);
	}
	
	public Document createEntity(Document doc) throws Exception{
		
		id=Test.getElementId(doc);
		Test.incElementId();
		
		//get the root element
        Element rootElement = doc.getDocumentElement();
        
        e=doc.createElement("Relationship");
        e.setAttribute("name", name);
        e.setAttribute("iri", iri);
        e.setAttribute("nary", nAry);
        e.setAttribute("id", id);
        e.setIdAttribute("id", true);
        rootElement.appendChild(e);
        
        
        //create the roles
        Role role1=new Role("r1");
        Element r1=role1.createEntity(doc);
        
        Role role2=new Role("r2");
        Element r2=role2.createEntity(doc);
        
        //reference the roles
        
        /*Element ref1=doc.createElement("Role");
        ref1.setAttribute("refName", role1.getName());
        ref1.setAttribute("ref", role1.getId());
        
        Element ref2=doc.createElement("Role");
        ref2.setAttribute("refName", role2.getName());
        ref2.setAttribute("ref", role2.getId());
        */
        
        e.appendChild(r1);
        e.appendChild(r2);
        return doc;
	}
	

	public String getIri() {
		return iri;
	}

	public void setIri(String iri) {
		this.iri = iri;
	}

	public Element getE() {
		return e;
	}

	public void setE(Element e) {
		this.e = e;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getnAry() {
		return nAry;
	}

	public void setnAry(String nAry) {
		this.nAry = nAry;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	
}
