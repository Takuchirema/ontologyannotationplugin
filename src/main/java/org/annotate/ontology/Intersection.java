package org.annotate.ontology;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.xml.sax.*;
import org.w3c.dom.*;

public class Intersection {
	
	private String name;
	private String id;
	private String tense="";
	private String roleCase="";
	private String hasPre="";
	private String rolePre="";
	
	public Intersection(String name){
		this.name=name;
	}
	
	public Document createEntity(Document doc){
		
		id=Test.getElementId(doc);
		
		//get the root element
        Element rootElement = doc.getDocumentElement();
        
        Element e=doc.createElement("Constructor");
        e.setAttribute("name", name);
        e.setAttribute("id",id);
        e.setIdAttribute("id", true);
        e.setAttribute("tense", tense);
        e.setAttribute("case", roleCase);
        e.setAttribute("hasPRE", "false");
        e.setAttribute("PRE", tense);
        rootElement.appendChild(e);
		
        return doc;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTense() {
		return tense;
	}

	public void setTense(String tense) {
		this.tense = tense;
	}

	public String getRoleCase() {
		return roleCase;
	}

	public void setRoleCase(String roleCase) {
		this.roleCase = roleCase;
	}

	public String getHasPre() {
		return hasPre;
	}

	public void setHasPre(String hasPre) {
		this.hasPre = hasPre;
	}

	public String getRolePre() {
		return rolePre;
	}

	public void setRolePre(String rolePre) {
		this.rolePre = rolePre;
	}
	
	
}
