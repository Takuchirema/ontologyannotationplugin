package org.annotate.ontology;

import java.util.ArrayList;
import org.apache.log4j.Logger;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

import org.xml.sax.*;

import edu.stanford.bmir.protege.examples.view.ExampleViewComponent;

import org.w3c.dom.*;

public class Entity {
	
	private String iri;
	private String name="";
	private String id="";
	private String gender="";
	private String nounClass="";
	private String grammNumber="";
	private Element e;
	public static final Logger log = Logger.getLogger(ExampleViewComponent.class);
	
	public Entity(String iri){
		this.iri=iri;
		this.name=iri.substring(iri.indexOf("#")+1,iri.length());
		log.info("Entity name: "+name);
	}
	
	public Document createEntity(Document doc) throws Exception{
		
		id=Test.getElementId(doc);
		
		//Document doc=Test.getDocument();
		//get the root element
        Element rootElement = doc.getDocumentElement();
        
        e=doc.createElement("Entity");
        e.setAttribute("iri", iri);
        e.setAttribute("name", name);
        e.setAttribute("id", id);
        e.setIdAttribute("id", true);
        e.setAttribute("nounClass", nounClass);
        e.setAttribute("grammNumber", grammNumber);
        e.setAttribute("gender", gender);
        
        rootElement.appendChild(e);
		
        return doc;
	}
	
	
	public String getIri() {
		return iri;
	}

	public void setIri(String iri) {
		this.iri = iri;
	}

	public void createSubElement(Element subElement){
		e.appendChild(subElement);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getNounClass() {
		return nounClass;
	}

	public void setNounClass(String nounClass) {
		this.nounClass = nounClass;
	}

	public String getGrammNumber() {
		return grammNumber;
	}

	public void setGrammNumber(String grammNumber) {
		this.grammNumber = grammNumber;
	}
	
	public Element getE() {
		return e;
	}

	public void setE(Element e) {
		this.e = e;
	}

}
