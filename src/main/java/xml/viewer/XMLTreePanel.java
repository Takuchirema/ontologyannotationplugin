package xml.viewer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import edu.stanford.bmir.protege.examples.view.SelectedObjectPanel;

public class XMLTreePanel extends JPanel {

	private static JTree tree;
	private XMLTreeModel model;
	private HashMap<String,TreePath> nodePaths=new HashMap<String,TreePath>();
	
	public XMLTreePanel() {
		setLayout(new BorderLayout());
		
		model = new XMLTreeModel();
		tree = new JTree();
		tree.setModel(model);
		tree.setShowsRootHandles(true);
		tree.setEditable(true);
		tree.setExpandsSelectedPaths(true);
		Font currentFont = tree.getFont();
		Font bigFont = new Font(currentFont.getName(), currentFont.getStyle(), 20);
		tree.setFont(bigFont);
		tree.setCellRenderer( new CustomTreeRenderer() );
		
		/*DefaultTreeCellRenderer renderer = (DefaultTreeCellRenderer) tree.getCellRenderer();
        renderer.setTextSelectionColor(Color.white);
        renderer.setBackgroundSelectionColor(Color.blue);
        renderer.setBorderSelectionColor(Color.black);*/
        
		JScrollPane pane = new JScrollPane(tree);
		pane.setPreferredSize(new Dimension(300,400));

		add(pane, "Center");
		
		final JTextField text = new JTextField();
		text.setEditable(false);
		add(text, "South");
		
		tree.addTreeSelectionListener(new TreeSelectionListener() {
			public void valueChanged(TreeSelectionEvent e) {
				Object lpc = e.getPath().getLastPathComponent();
				if (lpc instanceof XMLTreeNode) {
					text.setText( ((XMLTreeNode)lpc).getText() );
				}
				tree.invalidate();
				tree.validate();
				tree.repaint();
				/* Get the node currently selected */
				XMLTreeNode node = (XMLTreeNode)tree.getLastSelectedPathComponent();
				//System.out.println("Node Clicked ID: "+node.getElement().getAttributes().getNamedItem("id").getNodeValue());
				//System.out.println("Node Values: "+node.getText());
				
				XMLTreeNodePanel frame=new XMLTreeNodePanel(node);
				DemoMain.setPanel(frame);
				SelectedObjectPanel.setRightPanel();
			}
		});

	}
	
	public TreePath getPath(XMLTreeNode root) {
		TreePath path=null;
		//System.out.println("In create Node HashMap");
		//NodeList nl=root.getElementsByTagName("*");
	    @SuppressWarnings("unchecked")
	    Enumeration<DefaultMutableTreeNode> e = root.depthFirstEnumeration();
	    while (e.hasMoreElements()) {
	    	
	        DefaultMutableTreeNode node = e.nextElement();
	        TreePath nodePath = new TreePath(node.getPath());
	        
	        XMLTreeNode treeNode=(XMLTreeNode)nodePath.getLastPathComponent();
	        //System.out.println("Enumerating "+treeNode.getElement().getTagName()+" path: "+nodePath.toString());
	        if (hasAttribute(treeNode.getElement(),"iri")){
	        	//System.out.println("Has iri: "+treeNode.getElement().getTagName()+" "+nodePath.toString());
	        	path=nodePath;
	        }
	    }
	    
	    return path;
	}
	
	public void getNodeHashMap(XMLTreeModel model, Object object) {
		if (object instanceof XMLTreeNode) {
			XMLTreeNode node =(XMLTreeNode)object;
			TreePath path = getPath(node);
			if (path!=null){
				String iri = node.getElement().getAttributes().getNamedItem("iri").getNodeValue();
				nodePaths.put(iri, path);
			}
		}
		
	    for (int i = 0; i < model.getChildCount(object); i++) {
	        getNodeHashMap(model, model.getChild(object, i));
	    }

	}
	
	public boolean hasAttribute(Node element, String value) {
	    NamedNodeMap attributes = element.getAttributes();
	    for (int i = 0; i < attributes.getLength(); i++) {
	        Node node = attributes.item(i);
	        if (value.equals(node.getNodeName())) {
	            return true;
	        }
	    }
	    return false;
	}
	
	public void selectElement(String iri){
		try{
			tree.expandPath(nodePaths.get(iri));
			tree.setSelectionPath(nodePaths.get(iri));
			
			tree.invalidate();
			tree.validate();
			tree.repaint();
			//tree.scrollPathToVisible(nodePaths.get(iri));
			//tree.makeVisible(nodePaths.get(iri));
			
		}catch (Exception e){
			System.out.println(e.getMessage());
		}
	}
	
	public static JTree getTree() {
		return tree;
	}

	public void setTree(JTree tree) {
		this.tree = tree;
	}

	public XMLTreeModel getModel() {
		return model;
	}

	public void setModel(XMLTreeModel model) {
		this.model = model;
	}

	public HashMap<String, TreePath> getNodePaths() {
		return nodePaths;
	}

	public void setNodePaths(HashMap<String, TreePath> nodePaths) {
		this.nodePaths = nodePaths;
	}

	/* methods that delegate to the custom model */
	public void setDocument(Document document) {
		model.setDocument(document);
		getNodeHashMap(model,(XMLTreeNode)model.getRoot());
	}
	
	public Document getDocument() {
		return model.getDocument();
	}
	
	public void setSelectionPath(TreePath path){
		tree.setSelectionPath(path);
	}
}
