package xml.viewer;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

class MyTreeModelListener implements TreeModelListener {
	
	public void treeNodesChanged(TreeModelEvent e) {
		XMLTreeNode node;
		node = (XMLTreeNode) (e.getTreePath().getLastPathComponent());

		/*try {
			int index = e.getChildIndices()[0];
			node = (DefaultMutableTreeNode) (node.getChildAt(index));
		} catch (NullPointerException exc) {
			
		}*/

		System.out.println("Node Clicked: "+node.getElement());
		System.out.println("Node Values: "+node.getText());
	}

	public void treeNodesInserted(TreeModelEvent e) {
		System.out.println(e);
	}

	public void treeNodesRemoved(TreeModelEvent e) {
		System.out.println(e);
	}

	public void treeStructureChanged(TreeModelEvent e) {
		System.out.println(e);
	}
}
