package xml.viewer;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.tree.TreePath;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import edu.stanford.bmir.protege.examples.view.SelectedObjectPanel;

public class DemoMain extends JInternalFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1617281456861701120L;
	private static Document document = null;
	private static XMLTreeNodePanel nodePanel;
	private static HashMap<String,TreePath> nodePaths=new HashMap<String,TreePath>();
	private static XMLTreePanel panel;
	private static String documentPath;
	private static String path=
			"C:/Users/User/Desktop/computer_science/VacWork/June2016/Output/owl_xml.xml";
	
	/*
	public static void main(String[] args) throws Exception{
		
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = dbFactory.newDocumentBuilder();
		Document doc = builder.parse(new File(path));
		doc.normalize();
		
		new DemoMain(doc);
	}
	*/
	public DemoMain(Document doc) {
		document=doc;
		
		panel = new XMLTreePanel();
		panel.setDocument(document);
		nodePaths=panel.getNodePaths();
		
		//add(panel,"Center");
		getContentPane().add(panel, "Center");
		pack();
		setDefaultCloseOperation(JInternalFrame.EXIT_ON_CLOSE);
		setTitle("Positionalist Ontology View");
		//setLocationRelativeTo(null);
		setVisible(true);
	}
	
	public static void selectElement(String iri){
		panel.selectElement(iri);
	}
	
	public static HashMap<String, TreePath> getNodePaths() {
		return nodePaths;
	}

	public static void setNodePaths(HashMap<String, TreePath> nodePaths) {
		DemoMain.nodePaths = nodePaths;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public static void setDocument(Document document) {
		DemoMain.document = document;
	}

	public static Document getDocument(){
		return document;
	}

	public static XMLTreeNodePanel getPanel() {
		return nodePanel;
	}

	public static void setPanel(XMLTreeNodePanel nodePanel) {
		DemoMain.nodePanel = nodePanel;
		System.out.println("Demo panel set! ");
	}
	
	public static void setDocumentPath(String path){
		documentPath=path;
	}
	
}
