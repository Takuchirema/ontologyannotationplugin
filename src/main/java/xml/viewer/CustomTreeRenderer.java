package xml.viewer;

//Imports
import javax.swing.*;
import javax.swing.tree.*;
import java.awt.*;
import java.net.URL;

public class CustomTreeRenderer extends JLabel implements TreeCellRenderer {
	
	private ImageIcon entity;
	private ImageIcon relationship;
	private ImageIcon constructor;
	private ImageIcon role;
	private ImageIcon unknown;
	private ImageIcon subClass;
	private ImageIcon allPattern;
	private ImageIcon somePattern;
	private ImageIcon unionPattern;
	private ImageIcon intersectionPattern;
	private ImageIcon equivalence;
	private ImageIcon complement;
	private ImageIcon disjoint;
	
	private boolean bSelected;
	private int width=20;
	private int height=20;

	public CustomTreeRenderer()
	{
		// Load the images
		URL url = DemoMain.class.getResource("/e.png");
		entity = new ImageIcon(url);
		
		url = DemoMain.class.getResource("/r.png");
		relationship = new ImageIcon(url);
		
		url = DemoMain.class.getResource("/c.png");
		constructor = new ImageIcon(url);
		
		url = DemoMain.class.getResource("/role.png");
		role = new ImageIcon(url);
		
		url = DemoMain.class.getResource("/p.png");
		allPattern = new ImageIcon(url);
		
		url = DemoMain.class.getResource("/p.png");
		somePattern = new ImageIcon(url);
		
		url = DemoMain.class.getResource("/p.png");
		unionPattern = new ImageIcon(url);
		
		url = DemoMain.class.getResource("/p.png");
		intersectionPattern = new ImageIcon(url);
		
		url = DemoMain.class.getResource("/eq.png");
		equivalence = new ImageIcon(url);
		
		url = DemoMain.class.getResource("/comp.png");
		complement = new ImageIcon(url);
		
		url = DemoMain.class.getResource("/s.png");
		subClass = new ImageIcon(url);
		
		url = DemoMain.class.getResource("/d.png");
		disjoint = new ImageIcon(url);
		
		url = DemoMain.class.getResource("/unknown.png");
		unknown = new ImageIcon(url);
		
		/*entity = new ImageIcon( "/e.gif" );
		relationship = new ImageIcon( "/r.png" );
		constructor = new ImageIcon( "/c.png" );
		role = new ImageIcon( "/p.png" );
		unknown = new ImageIcon( "/unknown.png" );
		*/
		
		entity=resizeImage(entity);
		relationship=resizeImage(relationship);
		constructor=resizeImage(constructor);
		role=resizeImage(role);
		subClass=resizeImage(subClass);
		unknown=resizeImage(unknown);
		allPattern=resizeImage(allPattern);
		somePattern=resizeImage(somePattern);
		unionPattern=resizeImage(unionPattern);
		intersectionPattern=resizeImage(intersectionPattern);
		equivalence=resizeImage(equivalence);
		complement=resizeImage(complement);
		disjoint=resizeImage(disjoint);
	}

	public Component getTreeCellRendererComponent( JTree tree,
					Object value, boolean bSelected, boolean bExpanded,
					boolean bLeaf, int iRow, boolean bHasFocus ) {
		
		//System.out.println(" Rendering !!!");
		// Find out which node we are rendering and get its text
		XMLTreeNode node = (XMLTreeNode)value;
		String	labelText = node.tagName();

		this.bSelected = bSelected;
		
		// Set the correct foreground color
		if( !bSelected )
			setForeground( Color.black );
		else
			setForeground( Color.yellow );

		// Determine the correct icon to display
		if( labelText.equals( "Entity" ) )
			setIcon( entity );
		else if( labelText.equals( "RelationshipRef" ) )
			setIcon( relationship);
		else if( labelText.equals( "Relationship" ) )
			setIcon( relationship);
		else if( labelText.equalsIgnoreCase( "Role" ) )
			setIcon( role);
		else if( labelText.equalsIgnoreCase( "RoleRef" ) )
			setIcon( role );
		else if( labelText.equals( "Constructor" ) )
			setIcon( constructor);
		else if( labelText.equals( "SubClassOf" ) )
			setIcon(subClass);
		else if( labelText.equals( "ObjectSomeValuesFrom" ) )
			setIcon(somePattern);
		else if( labelText.equals( "ObjectAllValuesFrom" ) )
			setIcon(allPattern);
		else if( labelText.equals( "ObjectUnionOf" ) )
			setIcon(unionPattern);
		else if( labelText.equals( "ObjectIntersectionOf" ) )
			setIcon(intersectionPattern);
		else if( labelText.equals( "EquivalentClasses" ) )
			setIcon(equivalence);
		else if( labelText.equals( "ObjectComplementOf" ) )
			setIcon(complement);
		else if( labelText.equals( "DisjointClasses" ) )
			setIcon(disjoint);
		else
			setIcon(unknown);
		
		// Add the text to the cell
		setText( node.toString() );

		return this;
	}

	// This is a hack to paint the background.  Normally a JLabel can
	// paint its own background, but due to an apparent bug or
	// limitation in the TreeCellRenderer, the paint method is
	// required to handle this.
	public void paint( Graphics g )
	{
		Color		bColor;
		Icon		currentI = getIcon();

		// Set the correct background color
		bColor = bSelected ? SystemColor.textHighlight : Color.white;
		g.setColor( bColor );

		// Draw a rectangle in the background of the cell
		g.fillRect( 0, 0, getWidth() - 1, getHeight() - 1 );

		super.paint( g );
	}
	
	public ImageIcon resizeImage(ImageIcon imgIcon){
		Image img = imgIcon.getImage();
		Image newimg = img.getScaledInstance(width, height,  java.awt.Image.SCALE_SMOOTH);
		imgIcon = new ImageIcon(newimg);
		
		return imgIcon;
	}

}
