package xml.viewer;

import java.awt.BorderLayout;
import org.apache.log4j.Logger;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import edu.stanford.bmir.protege.examples.view.ExampleViewComponent;
import edu.stanford.bmir.protege.examples.view.SelectedObjectPanel;

public class XMLTreeNodePanel extends JInternalFrame{
	
	private XMLTreeNode node;
	private Element element;
	
	private JTextField textField;
    private JPanel panel;
    private JButton save;
    private JButton addProperty;
	private int cbWidth=200;
	private int cbHeight=30;
	
	private JButton refresh;
    private JButton saveXML;
    private JButton loadXML;
    private static final Logger log = Logger.getLogger(ExampleViewComponent.class);
    
	public XMLTreeNodePanel(XMLTreeNode node){
		this.node=node;
		
		try{
			this.element=node.getElement();
			if (!hasAttribute(element,"ref")){
				setFrame();
			}else{
				JPanel innerPanel = innerFrame();
				setRefEntity();
				panel.add(innerPanel);
			}
		}catch (Exception ex){}
		
	}
	
	public void setRefEntity(){
		String refId=element.getAttributes().getNamedItem("ref").getNodeValue();
		log.info("References: "+refId);
		
		element=(Element)getElementById(refId,DemoMain.getDocument());
		setFrame();
	}
	
	public JPanel innerFrame(){
		String title=element.getTagName();
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        setTitle(title);
        setResizable(false);

        JPanel innerPanel = new JPanel();
        
        
        
        innerPanel.setToolTipText("Top Element is the Referenced Element. This is the "
        		+ "Referencing XML Element");
        innerPanel.setLayout(new BoxLayout(innerPanel, BoxLayout.Y_AXIS));
        
        
        add(innerPanel);
        
        final Box box = Box.createVerticalBox();
        
        JPanel att = new JPanel(new BorderLayout());
        att.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        JLabel lbl = new JLabel("Top Element is the Referenced Element");

        lbl.setPreferredSize(new Dimension(195,10));
        att.add(lbl,BorderLayout.CENTER);
        box.add(att);
        
        /** Get attributes and put them in textFields for editing */
	    NamedNodeMap attributes = element.getAttributes();
	    for (int i = 0; i < attributes.getLength(); i++) {
	        Node node = attributes.item(i);
	        
	        JPanel attPanel = new JPanel(new BorderLayout());
	        attPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
	        
	        String nodeName=node.getNodeName();
	        JLabel lblLeft = new JLabel(nodeName);
	        Font font = new Font("Courier", Font.PLAIN,15);
	        lblLeft.setFont(font);
	
	        lblLeft.setPreferredSize(new Dimension(195,10));
	        
	        textField = new JTextField(15);
	        
	        textField.setFont(textField.getFont().deriveFont(15f));
	        textField.setText(node.getNodeValue());
	        textField.setEditable(false);
	        attPanel.add(textField,BorderLayout.EAST);

	        attPanel.add(lblLeft,BorderLayout.WEST);
	        attPanel.setMaximumSize(attPanel.getPreferredSize());
	        box.add(attPanel);
	         
	    }
	    
	    innerPanel.add(box);
	    
	    return innerPanel;
	}
	
    public void setFrame() {
    	String title=element.getTagName();
        //setSize(300, 300);  // better to use pack() (after components added)
        //setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
        
        // better to use
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        //setLocationRelativeTo(null);  // better to use..
        
        //setLocationByPlatform(true);
        setTitle(title);
        setResizable(false);
        //setLayout(null); // better to use layouts with padding & borders

        // set a flow layout with large hgap and vgap.
        panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        // panel.setBounds(5, 5, 290, 290); // better to pack()
        add(panel);
        
        final Box box = Box.createVerticalBox();
        
        /** Get attributes and put them in textFields for editing */
	    NamedNodeMap attributes = element.getAttributes();
	    for (int i = 0; i < attributes.getLength(); i++) {
	        Node node = attributes.item(i);
	        
	        JPanel attPanel = new JPanel(new BorderLayout());
	        attPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
	        
	        String nodeName=node.getNodeName();
	        JLabel lblLeft = new JLabel(nodeName);
	        Font font = new Font("Courier", Font.PLAIN,15);
	        lblLeft.setFont(font);
	        // Set the size of the label
	        lblLeft.setPreferredSize(new Dimension(195,10));// Width, Height
	        textField = new JTextField(15);
	        
	        if (nodeName.equalsIgnoreCase("id") || nodeName.equalsIgnoreCase("iri")){
		        
		        textField.setFont(textField.getFont().deriveFont(15f));
		        textField.setText(node.getNodeValue());
		        textField.setEditable(false);
		        attPanel.add(textField,BorderLayout.EAST);
	        	
	        }else if (nodeName.equalsIgnoreCase("hasPRE")){
	        	
	        	String[] choices = { "True","False"};
	            final JComboBox<String> cb = new JComboBox<String>(choices);
	            cb.setFont(textField.getFont().deriveFont(15f));
	            
	            cb.setSelectedItem(node.getNodeValue());
	            cb.setPreferredSize(new Dimension(cbWidth,cbHeight));
	            cb.setVisible(true);
	            
	            attPanel.add(cb,BorderLayout.EAST);
	        }else if (nodeName.equalsIgnoreCase("tense")){
	        	
	        	String[] choices = { "Active","Passive"};
	            final JComboBox<String> cb = new JComboBox<String>(choices);
	            cb.setFont(textField.getFont().deriveFont(15f));
	            
	            cb.setSelectedItem(node.getNodeValue());
	            cb.setPreferredSize(new Dimension(cbWidth,cbHeight));
	            cb.setVisible(true);
	            
	            attPanel.add(cb,BorderLayout.EAST);
	        		
	        }else if (nodeName.equalsIgnoreCase("nounClass")){
	        	
	        	String[] choices = { "1","1a","2","2a","3","3a","4","5","6","7","8","9",
	        						 "10","11","12","13","14","15","16","17","18","19",
	        						 "20","21","22","23","24","25"};
	            final JComboBox<String> cb = new JComboBox<String>(choices);
	            cb.setFont(textField.getFont().deriveFont(15f));
	            
	            cb.setSelectedItem(node.getNodeValue());
	            cb.setPreferredSize(new Dimension(cbWidth,cbHeight));
	            cb.setVisible(true);
	            
	            attPanel.add(cb,BorderLayout.EAST);
	        	
	        }else if (nodeName.equalsIgnoreCase("case")){
	        	
	        	String[] choices = { "Nominative","Accusative","Dative","Genitive"};
	            final JComboBox<String> cb = new JComboBox<String>(choices);
	            cb.setFont(textField.getFont().deriveFont(15f));
	            
	            cb.setSelectedItem(node.getNodeValue());
	            cb.setPreferredSize(new Dimension(cbWidth,cbHeight));
	            cb.setVisible(true);
	            
	            attPanel.add(cb,BorderLayout.EAST);
	        }else if (nodeName.equalsIgnoreCase("grammNumber")){
	        	
	        	String[] choices = { "Singular","Plural","Dual","Trial","Quadrial","Mass","Collective"};
	            final JComboBox<String> cb = new JComboBox<String>(choices);
	            cb.setFont(textField.getFont().deriveFont(15f));
	            
	            cb.setSelectedItem(node.getNodeValue());
	            cb.setPreferredSize(new Dimension(cbWidth,cbHeight));
	            cb.setVisible(true);
	            
	            attPanel.add(cb,BorderLayout.EAST);
	        }else if (nodeName.equalsIgnoreCase("gender")){
	        	
	        	String[] choices = { "Masculine","Neuter","Feminine"};
	            final JComboBox<String> cb = new JComboBox<String>(choices);
	            cb.setFont(textField.getFont().deriveFont(15f));
	            cb.setSelectedItem(node.getNodeValue());
	            cb.setPreferredSize(new Dimension(cbWidth,cbHeight));
	            cb.setVisible(true);
	            attPanel.add(cb,BorderLayout.EAST);
	        }else{
	        	// suggest a size in columns
		        textField = new JTextField(15);
		        //textField.setBounds(5, 5, 280, 50); 
		        
		        textField.setFont(textField.getFont().deriveFont(15f));
		        textField.setText(node.getNodeValue());
		        
		        attPanel.add(textField,BorderLayout.EAST);
	        	
	        }
	        
	        
	        attPanel.add(lblLeft,BorderLayout.WEST);
	        attPanel.setMaximumSize(attPanel.getPreferredSize());
	        box.add(attPanel);
	         
	    }
	    
	    JPanel attPanel = new JPanel(new BorderLayout());
        attPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        
        save = new JButton("Save");
        addProperty = new JButton("Add Property");
        
        attPanel.add(save,BorderLayout.EAST);
        attPanel.add(addProperty,BorderLayout.WEST);
        attPanel.setMaximumSize(attPanel.getPreferredSize());
        
	    panel.add(box);
	    panel.add(attPanel,BorderLayout.SOUTH);
        
        save.addActionListener(new ActionListener() {
        	
			@Override
			public void actionPerformed(ActionEvent e) {
				for (Component c : box.getComponents()) {
					String label="";
					String value="";
					
				    if (c instanceof JPanel) { 
				    	JPanel p = (JPanel)c;
				    	for (Component sc : p.getComponents()){
				    		if (sc instanceof JLabel){
				    			label = ((JLabel)sc).getText();
				    		}else if (sc instanceof JTextField){
				    			JTextField textField=(JTextField)sc;
				    			if (textField.getName()!=null)
				    				label=((JTextField)sc).getText();
				    			else
				    				value=((JTextField)sc).getText();
				    		}else if (sc instanceof JComboBox){
				    			value=(String)((JComboBox<String>)sc).getSelectedItem();
				    		}
				    	}
				    	value=value.replaceAll("[^A-Za-z0-9]", "");
				    	label=label.replaceAll("[^A-Za-z0-9]", "");
				    	log.info(value+" "+label);
				    	element.setAttribute(label, value);
				    	log.info("elemet att: "+element.getAttributes()
				    		.getNamedItem(label).getNodeValue());
				    }
				}
			}
			
        });
        
        addProperty.addActionListener(new ActionListener() {
        	
			@Override
			public void actionPerformed(ActionEvent e) {
				JPanel attPanel = new JPanel(new BorderLayout());
		        attPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		        
		        JTextField lblLeft = new JTextField(5);
		        Font font = new Font("Courier", Font.PLAIN,15);
		        lblLeft.setFont(font);
		        lblLeft.setText("attribute_name (alphanumeric)");
		        lblLeft.setBackground(null);
		        lblLeft.setBorder(null);
		        lblLeft.setName("label");
		        lblLeft.setColumns(15);
		        
		        // suggest a size in columns
		        textField = new JTextField(15);
		        //create new Font
                textField.setFont(textField.getFont().deriveFont(15f));
		        textField.setText("Attribute Value");
		        
		        attPanel.add(textField,BorderLayout.EAST);
		        attPanel.add(lblLeft,BorderLayout.WEST);
		        attPanel.setMaximumSize(attPanel.getPreferredSize());
		       
		        box.add(attPanel);
		        pack();
		        log.info("After pack!!");
			}
			
        });
        
	    
        pack(); // make the GUI the minimum size needed to display the content
        setVisible(true);
    }
    
    public static void saveFile(String path) throws Exception{
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
		
		// Output to console for testing
		Result output = new StreamResult(new File(path));
		Source input = new DOMSource(DemoMain.getDocument());
		
		transformer.transform(input, output);
	}
    
    public static void loadFile(String path) throws Exception{
    	
    	try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = dbFactory.newDocumentBuilder();
			Document doc = builder.parse(new File(path));
			doc.normalize();
			
			SelectedObjectPanel.reloadXMLTree(doc);
		}
		catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
    
    public Node getElementById(String id,Document doc) {
    	
	    Node node=null;
	    NodeList nl;
	    nl = doc.getElementsByTagName("*");
	    log.info("number of elemnts"+nl.getLength());
	    if (nl.getLength() > 0) {
	    	loop:
	    	for (int i=0;i<nl.getLength();i++){
	    		if (hasAttribute(nl.item(i),"id")){
	    			
	    			String nodeId=nl.item(i).getAttributes().getNamedItem("id").getNodeValue();
	    			if (nodeId.equals(id)){
		    			log.info("Found Node!!");
		    			node=nl.item(i);
		    			break loop;
	    			}
	    		}
	    	}
	    }
	    return node;
	}
    
    public static boolean hasAttribute(Node element, String value) {
	    NamedNodeMap attributes = element.getAttributes();
	    for (int i = 0; i < attributes.getLength(); i++) {
	        Node node = attributes.item(i);
	        if (value.equals(node.getNodeName())) {
	            return true;
	        }
	    }
	    return false;
	}


}
