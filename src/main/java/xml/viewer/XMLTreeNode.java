package xml.viewer;

import java.util.Vector;

import javax.swing.tree.DefaultMutableTreeNode;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public class XMLTreeNode extends DefaultMutableTreeNode{
	
	Element element;
	
	public XMLTreeNode(Element element) {
		this.element = element;
	}
	public Element getElement() {
		return element;
	}
	
	public String toString() {
		
		String nodeName="";
		
		if (hasAttribute(element,"ref")){
			String refId=element.getAttributes().getNamedItem("ref").getNodeValue();
			Element refElement=(Element)getElementById(refId,DemoMain.getDocument());
			if (hasAttribute(refElement,"name")){
				nodeName=refElement.getAttributes().getNamedItem("name").getNodeValue();
				return nodeName;
			}
		}
		
		if (element.getNodeName().equals("ObjectSomeValuesFrom")){
			nodeName="SomeValuesFrom";
		}else if (element.getNodeName().equals("ObjectAllValuesFrom")){
			nodeName="AllValuesFrom";
		}else if (hasAttributeName(element,"name")){
			nodeName=element.getAttributes().getNamedItem("name").getNodeValue();
		}else if (hasAttributeName(element,"refName")){
			nodeName=element.getAttributes().getNamedItem("refName").getNodeValue();
		}else{
			nodeName=element.getNodeName();
		}
		
		return nodeName;
	}
	
	public String tagName(){
		return element.getNodeName();
	}
	
	public int getChildCount(){
		Vector<Element> elements = children( element);
		return elements.size();
	}
	
	public String getText() {
		NodeList list = element.getChildNodes();
		for (int i=0 ; i<list.getLength() ; i++) {
			if (list.item(i) instanceof Text) {
				return ((Text)list.item(i)).getTextContent();
			}
		}
		return "";
	}
	
	public Vector<Element> children(Node node) {
		Vector<Element> elements = new Vector<Element>();
		NodeList list = node.getChildNodes();
		for (int i=0 ; i<list.getLength() ; i++) {
			if (list.item(i).getNodeType() == Node.ELEMENT_NODE) {
				elements.add( (Element) list.item(i));
			}
		}
		return elements;
	}
	
	public static boolean hasAttributeName(Node element, String value) {
	    NamedNodeMap attributes = element.getAttributes();
	    for (int i = 0; i < attributes.getLength(); i++) {
	        Node node = attributes.item(i);
	        if (value.equals(node.getNodeName())) {
	            return true;
	        }
	    }
	    return false;
	}
	
	public Node getElementById(String id,Document doc) {
    	
	    Node node=null;
	    NodeList nl;
	    nl = doc.getElementsByTagName("*");
	    
	    if (nl.getLength() > 0) {
	    	loop:
	    	for (int i=0;i<nl.getLength();i++){
	    		if (hasAttribute(nl.item(i),"id")){
	    			
	    			String nodeId=nl.item(i).getAttributes().getNamedItem("id").getNodeValue();
	    			if (nodeId.equals(id)){
		    	
		    			node=nl.item(i);
		    			break loop;
	    			}
	    		}
	    	}
	    }
	    return node;
	}
	
	public static boolean hasAttribute(Node element, String value) {
	    NamedNodeMap attributes = element.getAttributes();
	    for (int i = 0; i < attributes.getLength(); i++) {
	        Node node = attributes.item(i);
	        if (value.equals(node.getNodeName())) {
	            return true;
	        }
	    }
	    return false;
	}
}
