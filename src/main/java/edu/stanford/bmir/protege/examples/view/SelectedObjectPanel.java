package edu.stanford.bmir.protege.examples.view;

import org.annotate.ontology.AxiomAnalyser;
import org.apache.log4j.Logger;
import org.annotate.ontology.Test;
import org.protege.editor.owl.OWLEditorKit;
import org.protege.editor.owl.model.OWLModelManager;
import org.protege.editor.owl.model.OWLWorkspace;
import org.protege.editor.owl.model.event.EventType;
import org.protege.editor.owl.model.event.OWLModelManagerChangeEvent;
import org.protege.editor.owl.model.event.OWLModelManagerListener;
import org.protege.editor.owl.model.selection.OWLSelectionModelListener;
import org.protege.editor.owl.ui.OWLOntologyDisplayPanel;
import org.protege.editor.owl.ui.frame.OWLEntityFrame;
import org.protege.editor.owl.ui.framelist.OWLFrameList;
import org.protege.editor.owl.ui.usage.UsagePanel;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.util.OWLObjectVisitorAdapter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import xml.viewer.DemoMain;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.Set;
/*
 * Copyright (C) 2007, University of Manchester
 *
 *
 */


/**
 * Author: Matthew Horridge<br>
 * The University Of Manchester<br>
 * Bio-Health Informatics Group<br>
 * Date: 29-Oct-2007<br><br>
 *
 * A panel that displays information about selected objects (specifically
 * entities and ontologies) - just an experimental hack really.
 */
public class SelectedObjectPanel extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = -1836316447338285493L;

    private static OWLEditorKit owlEditorKit;
    private static final Logger log = Logger.getLogger(ExampleViewComponent.class);
    private static OWLOntology ontology;
    //private OWLEntityFrame entityFrame;

    //private OWLFrameList frameList;

    private CardLayout layout;

    private static JPanel cardPanel;

    private OWLOntologyDisplayPanel ontologyPanel;

    private JLabel objectDisplayLabel;

    private JCheckBox showUsageCheckBox;

    private static JSplitPane usageSplitPane;

    private UsagePanel usagePanel;
    
    private static PriorityQueue<OWLEntity> classQueue = new PriorityQueue<OWLEntity>();
    
    private static HashMap<IRI,EditOWLClass> owlClasses = new HashMap<IRI,EditOWLClass>();
    
    private static String documentPath =
    		"";
    
    private static DemoMain demo;
    
    private JButton refresh;
    private JButton saveXML;
    private JButton loadXML;
    private JButton generateXML;
    
    public SelectedObjectPanel(OWLEditorKit owlEditorKit) throws Exception {
        this.owlEditorKit = owlEditorKit;
        
        ontology=owlEditorKit.getModelManager().getActiveOntology();
        
        
        setLayout(new BorderLayout(3, 3));
        cardPanel = new JPanel(layout = new CardLayout());
        add(cardPanel);
        objectDisplayLabel = new JLabel();
        JPanel headerPanel = new JPanel(new BorderLayout(7, 7));
        headerPanel.add(objectDisplayLabel, BorderLayout.WEST);
        add(headerPanel, BorderLayout.NORTH);
        
        //ontology.getEntitiesInSignature("");
        showUsageCheckBox = new JCheckBox(new AbstractAction("Show XML Editor") {
        	
            private static final long serialVersionUID = -8105770102874054033L;

            public void actionPerformed(ActionEvent e) {
                showUsagePanel(showUsageCheckBox.isSelected());
            }
            
        });
        
        saveXML = new JButton("Save to XML");
        refresh = new JButton("Refresh");
        loadXML = new JButton("Load XML");
        generateXML = new JButton("Generate XML");
        
        saveXML.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				if (fileChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
				  File file = fileChooser.getSelectedFile();
				  // save to file
				  log.info("save to: "+file.getAbsolutePath());
				  
				  try {
					  SelectedObjectPanel.processEntities();
					  saveFile(file.getAbsolutePath());
				  } catch (Exception e1) {
					  // TODO Auto-generated catch block
					  e1.printStackTrace();
				  }
				  
				}
			}
        });
        
        loadXML.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
				  File file = fileChooser.getSelectedFile();
				  // save to file
				  log.info("open : "+file.getAbsolutePath());
				  
				  try {
					  SelectedObjectPanel.processEntities();
					  loadFile(file.getAbsolutePath());
				  } catch (Exception e1) {
					  // TODO Auto-generated catch block
					  e1.printStackTrace();
				  }
				  
				}
			}
        });
        
        refresh.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Document doc = DemoMain.getDocument();
				SelectedObjectPanel.reloadXMLTree(doc);
			}
        });
        
        generateXML.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					analyseAxioms(ontology);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
        });
        
        JPanel attPanel = new JPanel(new BorderLayout());
        attPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        attPanel.add(saveXML,BorderLayout.EAST);
        attPanel.add(loadXML,BorderLayout.CENTER);
        attPanel.add(refresh,BorderLayout.WEST);
        
        headerPanel.add(attPanel,BorderLayout.WEST);
        headerPanel.add(generateXML,BorderLayout.CENTER);
        headerPanel.add(showUsageCheckBox, BorderLayout.EAST);
        
        /* Add the XML tree viewer */
        demo = new DemoMain(createDocument(documentPath));
        cardPanel.add("ENTITIES",demo);
        
        createClasses();
        
        /* Entity Frame is the frame with the entities */
        //entityFrame = new OWLEntityFrame(getOWLEditorKit());
        //frameList = new OWLFrameList(getOWLEditorKit(), entityFrame);
        //cardPanel.add("ENTITIES", new JScrollPane(frameList));
        owlEditorKit.getWorkspace().getOWLSelectionModel().addListener(new OWLSelectionModelListener() {
        	@Override
			public void selectionChanged() throws Exception {
        		log.info("*** something selected");
			    OWLObject selObj = getOWLWorkspace().getOWLSelectionModel().getSelectedObject();
			    //frameList.setRootObject(selObj);
			    if (!classQueue.isEmpty())
		        	processEntities(classQueue);
			    
			    if (selObj instanceof OWLEntity) {
			        //layout.show(cardPanel, "ENTITIES");
			        
			        OWLEntity owlEntity=(OWLEntity)selObj;
			        String iri = owlEntity.getIRI().toString();
			        log.info("Entity IRI is: "+iri);
			        
			        DemoMain.selectElement(iri.toString());
			        classQueue.add(owlEntity);
			        //processEntity(owlEntity);
			        
			        
			    }
			    else if(selObj instanceof OWLOntology) {
			        ontologyPanel.setOntology((OWLOntology) selObj);
			        layout.show(cardPanel, "ONTOLOGY");
			        
			        OWLOntology owlOntology=(OWLOntology)selObj;
			        log.info("Object is Ontology");
			    }
			    updateLabel(selObj);
			}
		});
        
      
        //frameList.setRootObject(null);
        getOWLModelManager().addListener(new OWLModelManagerListener() {
        	@Override
			public void handleChange(OWLModelManagerChangeEvent event) {
        		log.info("*** got onto event!");
        		
        		try{
        			processEntities(classQueue);
        		}catch(Exception ex){
        			
        		}
        		ontology = getOWLModelManager().getActiveOntology();
        		
        		try {
					createClasses();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        		
			    if(event.isType(EventType.ACTIVE_ONTOLOGY_CHANGED) ||
			    		event.isType(EventType.ONTOLOGY_LOADED)||
			    		event.isType(EventType.ONTOLOGY_VISIBILITY_CHANGED)) {
			    	log.info("##### Change ######");
			    	
			    	try{
				        ontologyPanel.setOntology(ontology);
				        layout.show(cardPanel, "ONTOLOGY");
				        updateLabel(ontology);
			    	}catch (Exception ex){
			    		
			    	}
			        
			    }
			}
		});
        
        //cardPanel.add("ONTOLOGY", ontologyPanel = new OWLOntologyDisplayPanel(owlEditorKit));
        setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));

        //usagePanel = new UsagePanel(owlEditorKit);
        usageSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        usageSplitPane.setResizeWeight(0.9);
        usageSplitPane.setRightComponent(DemoMain.getPanel());
        
        if (DemoMain.getPanel()==null){
        	log.info("Demo main panel is null ");
        }else{
        	log.info("Demo main panel is not null ");
        }
    }
    
    public static void analyseAxioms(OWLOntology o) throws Exception{
    	// Iterate over the axioms of the ontology. There are more than just the subclass
        // axiom, because the class declarations are also axioms.  All in all, there are
        // four:  the subclass axiom and three declarations of named classes.
    	AxiomAnalyser a = new AxiomAnalyser("","");
    	Test.loadXMLDocument();
		Document newDocument = Test.getDocument();
		
    	a.setDocument(newDocument);
    	
        log.info( "== All Axioms: ==" );
        for ( final OWLAxiom axiom : o.getAxioms() ) {
            log.info("#########################\n"+axiom);
            //log.info( axiom );
            String currentAxiom = axiom.toString();
            
            
            a.analyse("",currentAxiom);
            
            //log.info("--------- document ----------");
            //printDocument();
            //log.info(" ----------------------------");
            
        }
        //saveFile("C:/Users/User/Desktop/computer_science/VacWork/June2016/Output/owl_xml.xml");
        reloadXMLTree(newDocument);
        
        infoBox("XML generated and loaded into viewer","XML Generate");
    }
    
    public static void infoBox(String infoMessage, String titleBar)
    {
        JOptionPane.showMessageDialog(null, infoMessage, "InfoBox: " + titleBar, JOptionPane.INFORMATION_MESSAGE);
    }
    
    public static void saveFile(String path) throws Exception{
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
		
		// Output to console for testing
		Result output = new StreamResult(new File(path));
		Source input = new DOMSource(DemoMain.getDocument());
		
		transformer.transform(input, output);
	}
    
    public static void loadFile(String path) throws Exception{
    	
    	try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = dbFactory.newDocumentBuilder();
			Document doc = builder.parse(new File(path));
			doc.normalize();
			
			SelectedObjectPanel.reloadXMLTree(doc);
		}
		catch (Exception e) {
			log.info("Could not load the file: "+path);
			log.info("Please Ensure it is an XML File");
			//System.exit(1);
		}
	}
    
    public static void processEntities(PriorityQueue<OWLEntity> queue) throws Exception{
    	
    	for (OWLEntity owlClass: queue){
    		processEntity(queue.poll());
    	}
    	
    }
    
    public static void reloadXMLTree(Document doc){
    	cardPanel.remove(demo);
    	
    	demo = new DemoMain(doc);
        cardPanel.add("ENTITIES",demo);
        
    }
    
    public static Document createDocument(String documentPath) throws Exception{
    	Document doc=null;
    	
    	try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = dbFactory.newDocumentBuilder();
			doc = builder.parse(new File(documentPath));
			doc.normalize();
		}
		catch (Exception e) {
			log.info("Loading Default Document");
			Test.loadXMLDocument();
			doc = Test.getDocument();
			//System.exit(1);
		}
    	return doc;
    }
    
    public static void processEntities() throws Exception{
    	
    	for (OWLEntity owlClass: classQueue){
    		processEntity(classQueue.poll());
    	}
    	
    }
    
    public static void processEntity(OWLEntity entity) throws Exception{
    	IRI iri = entity.getIRI();
    	
    	if (ontology==null){
    		log.info("onto null");
    	}
    	Set<OWLEntity> ontologyEntity = ontology.getEntitiesInSignature(iri);
    	
    	if (ontologyEntity.isEmpty() && docContainsElement(entity)){
    		owlClasses.get(iri).deleteFromXML();
    		owlClasses.remove(iri);
    	}else if (!ontologyEntity.isEmpty() && docContainsElement(entity) ){
    		owlClasses.get(iri).processClass();
    	}else if (!ontologyEntity.isEmpty() && !docContainsElement(entity)){
    		OWLEntity owlClass = entity;
    		EditOWLClass editClass = new EditOWLClass(owlClass);
    		owlClasses.put(iri, editClass);
    		editClass.addToXML();
    	}
    	
    }
    
    public static boolean docContainsElement (OWLEntity entity) throws Exception{
    	boolean contains=false;
    	String iri = entity.getIRI().toString();
    	
    	NodeList nl = null;
    	
    	try{
	    	Element root = demo.getDocument().getDocumentElement();
	    	
	    	if (entity instanceof OWLClass){
	    		nl = root.getElementsByTagName("Entity");	
	    	}else{
	    		nl = root.getElementsByTagName("Relationship");
	    	}
	    	
	    	if (nl !=null){
		    	loop:
				for (int i=0;i<nl.getLength();i++){
					
					String nodeIri = nl.item(i).getAttributes().getNamedItem("iri").getNodeValue();
					if (nodeIri.equals(iri)){
						contains=true;
						break loop;
					}
				}
	    	}

    	}catch (Exception ex){
    		log.info("Error: Document not loaded yet. SelectedObjectPanel.docContainsElement");
    	}
    	
    	return contains;
    }
    
    public static OWLOntology getOntology(){
    	return ontology;
    }
    
    private void showUsagePanel(boolean b) {
        if (b) {
            remove(cardPanel);
            usageSplitPane.setLeftComponent(cardPanel);
            add(usageSplitPane);
            revalidate();
        }
        else {
            remove(usageSplitPane);
            add(cardPanel);
            revalidate();
        }
    }

    private void updateLabel(final OWLObject object) {
        object.accept(new OWLObjectVisitorAdapter() {

            public void visit(OWLClass owlClass) {
                objectDisplayLabel.setText("<html><body><b>Class: </b>" + owlEditorKit.getModelManager().getRendering(object) + "</body></html>");
            }


            public void visit(OWLDataProperty owlDataProperty) {
                objectDisplayLabel.setText("<html><body><b>Data property: </b>" + owlEditorKit.getModelManager().getRendering(object) + "</body></html>");
            }


            public void visit(OWLIndividual owlIndividual) {
                objectDisplayLabel.setText("<html><body><b>Individual: </b>" + owlEditorKit.getModelManager().getRendering(object) + "</body></html>");
            }


            public void visit(OWLObjectProperty owlObjectProperty) {
                objectDisplayLabel.setText("<html><body><b>Object property: </b>" + owlEditorKit.getModelManager().getRendering(object) + "</body></html>");
            }


            public void visit(OWLOntology owlOntology) {
                objectDisplayLabel.setText("<html><body><b>Ontology: </b>" + owlEditorKit.getModelManager().getRendering(owlOntology) + "</body></html>");
            }
        });
    }
    
    public static void createClasses() throws Exception{
    	
    	Set<OWLClass> setClasses = ontology.getClassesInSignature();
    	Set<OWLObjectProperty> objectProperties=ontology.getObjectPropertiesInSignature();
    	
    	for (OWLClass owlClass: setClasses){
    		owlClasses.put(owlClass.getIRI(),new EditOWLClass(owlClass));
    		processEntity(owlClass);
    	}
    	
    	for (OWLObjectProperty owlClass: objectProperties){
    		owlClasses.put(owlClass.getIRI(),new EditOWLClass(owlClass));
    		processEntity(owlClass);
    	}
    	
    	updateDoc();
    	
    }
    
    /** If some nodes were deleted in the XML Document this updates it 
     * @throws Exception */
    public static void updateDoc() throws Exception{
    	
    	try {
	    	Element root = demo.getDocument().getDocumentElement();
	    	
	    	NodeList entityNl = root.getElementsByTagName("Entity");
	    	NodeList relationshipNl = root.getElementsByTagName("Relationship");
	    	
	    	for (int i=0;i<entityNl.getLength();i++){
	    		boolean contained=false;
	    		String iri = entityNl.item(i).getAttributes().getNamedItem("iri").getNodeValue();
	    		Set<OWLClass> classes = ontology.getClassesInSignature();
	    		loop:
	    		for (OWLClass owlClass:classes){
	    			if (owlClass.getIRI().toString().equals(iri)){
	    				contained=true;
	    				break loop;
	    			}
	    		}
	    		
	    		if (!contained){
	    			AxiomAnalyser analyser = new AxiomAnalyser("","");
	    			analyser.setDocument(DemoMain.getDocument());
	    			analyser.deleteEntity(iri);
	    		}
	    	}
	    	
	    	for (int i=0;i<relationshipNl.getLength();i++){
	    		boolean contained=false;
	    		String iri = relationshipNl.item(i).getAttributes().getNamedItem("iri").getNodeValue();
	    		Set<OWLObjectProperty> classes = ontology.getObjectPropertiesInSignature();
	    		loop:
	    		for (OWLObjectProperty owlClass:classes){
	    			if (owlClass.getIRI().toString().equals(iri)){
	    				contained=true;
	    				break loop;
	    			}
	    		}
	    		
	    		if (!contained){
	    			AxiomAnalyser analyser = new AxiomAnalyser("","");
	    			analyser.setDocument(DemoMain.getDocument());
	    			analyser.deleteEntity(iri);
	    		}
	    	}
    	}catch (Exception ex){
    		log.info("Error: No Ontology or XML loaded yet SelectedObjectPanel.updateDoc");
    	}
    	
    	
    }
    
    public static HashMap<IRI,EditOWLClass> getOwlClasses(){
    	return owlClasses;
    }
    
    public static OWLEditorKit getOWLEditorKit() {
        return owlEditorKit;
    }

    public OWLWorkspace getOWLWorkspace() {
        return getOWLEditorKit().getWorkspace();
    }

    public static OWLModelManager getOWLModelManager() {
        return getOWLEditorKit().getModelManager();
    }
    
    public static void setRightPanel(){
    	usageSplitPane.setRightComponent(DemoMain.getPanel());
    }
}
