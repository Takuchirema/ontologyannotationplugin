package edu.stanford.bmir.protege.examples.view;

import java.util.ArrayList;
import java.util.Set;

import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLOntology;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.annotate.ontology.*;
import org.apache.log4j.Logger;

import xml.viewer.DemoMain;
import xml.viewer.XMLTreeNode;
import org.apache.log4j.Logger;

public class EditOWLClass {
	
	private static final Logger log = Logger.getLogger(ExampleViewComponent.class);
	private OWLEntity owlClass;
	private IRI iri;
	private Set<OWLAxiom> axioms;
	private OWLOntology ontology;
	private AxiomAnalyser analyser;
	private int count=1;
	
	public EditOWLClass(OWLEntity owlClass){
		this.owlClass=owlClass;
		iri=owlClass.getIRI();
		ontology = SelectedObjectPanel.getOntology();
		axioms=owlClass.getReferencingAxioms(ontology);
		for (OWLAxiom axiom:axioms){
			//log.info("axiom number: "+count+"\naxiom:"+axiom.toString());
			//count++;
		}
	}
	
	public void processClass() throws Exception{
		log.info("Processing Class : "+owlClass.getIRI().toString());
		Set<OWLAxiom> newAxioms=owlClass.getReferencingAxioms(ontology);
		
		for (OWLAxiom axiom:newAxioms){
			//log.info("new axiom: "+axiom.toString());
			if (!axioms.contains(axiom)){
				analyser = new AxiomAnalyser(axiom.toString(),"");
				
				log.info("new axiom added! "+axiom.toString());
				
				XMLTreeNode treeNode = (XMLTreeNode)DemoMain.getNodePaths().get(iri.toString())
										.getLastPathComponent();
				Element element = treeNode.getElement();
				analyser.setDocument(DemoMain.getDocument());
				
				Element subElement=analyser.analyse("",axiom.toString());
				
				log.info("***** SubElement ******");
				analyser.printElement(subElement);
				log.info("*****After Element ******");
				analyser.printElement(element);
				log.info("***********");
				
				if (subElement != null){
					log.info("subElement not null");
					element.appendChild(subElement);
				}
				
				//analyser.printDocument();
				
				axioms.add(axiom);
			}
		}
		
		
		for (OWLAxiom axiom:axioms){
			if (!newAxioms.contains(axiom)){
				analyser = new AxiomAnalyser(axiom.toString(),"");
				
				log.info("axiom deleted! "+axiom.getAxiomType());
				
				XMLTreeNode treeNode = (XMLTreeNode)DemoMain.getNodePaths().get(iri.toString())
						.getLastPathComponent();
				Element element = treeNode.getElement();
				analyser.setDocument(DemoMain.getDocument());
				
				log.info("***** old element *****");
				analyser.printElement(element);
				
				analyser.deleteAxiom(element,axiom.toString());
				
				log.info("***** new element *****");
				analyser.printElement(element);
				
				axioms.remove(axiom);
			}
		}
	}
	
	public void addToXML() throws Exception{
		log.info("New Class Added!! "+iri.toString());
		
		analyser = new AxiomAnalyser("","");
		analyser.setDocument(DemoMain.getDocument());
		
		analyser.addEntity(owlClass);
		
		Document doc = DemoMain.getDocument();
		SelectedObjectPanel.reloadXMLTree(doc);
		
		Set<OWLAxiom> newAxioms=owlClass.getReferencingAxioms(ontology);
		
		for (OWLAxiom axiom:newAxioms){
			
			analyser = new AxiomAnalyser(axiom.toString(),"");
			
			try{
				log.info("class iri for axiom "+iri.toString());
				XMLTreeNode treeNode = (XMLTreeNode)DemoMain.getNodePaths().get(iri.toString())
										.getLastPathComponent();
				Element element = treeNode.getElement();
				analyser.setDocument(DemoMain.getDocument());
				
				Element subElement=analyser.analyse("",axiom.toString());
				
				if (subElement != null){
					log.info("subElement not null");
					element.appendChild(subElement);
				}
				
				if (!axioms.contains(axiom))
					axioms.add(axiom);
			
			}catch (Exception ex){
				
			}
			
		}
		
		//processClass();
	}
	
	public void deleteFromXML() throws Exception{
		log.info("Deleting Class !! "+iri.toString());
		
		analyser = new AxiomAnalyser("","");
		analyser.setDocument(DemoMain.getDocument());
		
		analyser.deleteEntity(owlClass.getIRI().toString());
	}

}
