package edu.stanford.bmir.protege.examples.view;


import org.protege.editor.owl.OWLEditorKit;
import org.protege.editor.owl.model.OWLModelManager;
import org.protege.editor.owl.model.OWLWorkspace;
import org.protege.editor.owl.model.event.EventType;
import org.protege.editor.owl.model.event.OWLModelManagerChangeEvent;
import org.protege.editor.owl.model.event.OWLModelManagerListener;
import org.protege.editor.owl.model.selection.OWLSelectionModelListener;
import org.protege.editor.owl.ui.OWLOntologyDisplayPanel;
import org.protege.editor.owl.ui.frame.OWLEntityFrame;
import org.protege.editor.owl.ui.frame.OWLFrameSection;
/* For buttons*/
import org.protege.editor.owl.ui.framelist.*;
import org.protege.editor.core.ui.*;
/* ************ */
import org.protege.editor.core.ui.list.MList;
import org.protege.editor.owl.ui.usage.UsagePanel;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.util.OWLObjectVisitorAdapter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;
/*
 * Copyright (C) 2007, University of Manchester
 *
 *
 */


/**
 * Author: Matthew Horridge<br>
 * The University Of Manchester<br>
 * Bio-Health Informatics Group<br>
 * Date: 29-Oct-2007<br><br>
 *
 * A panel that displays information about selected objects (specifically
 * entities and ontologies) - just an experimental hack really.
 */
public class SelectedObjectPanelTest extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = -1836316447338285493L;

    private OWLEditorKit owlEditorKit;

    private OWLEntityFrame entityFrame;

    private OWLFrameList frameList;

    private CardLayout layout;

    private JPanel cardPanel;

    private OWLOntologyDisplayPanel ontologyPanel;

    private JLabel objectDisplayLabel;

    private JCheckBox showUsageCheckBox;

    private JSplitPane usageSplitPane;

    private UsagePanel usagePanel;


    public SelectedObjectPanelTest(OWLEditorKit owlEditorKit) {
        this.owlEditorKit = owlEditorKit;
        setLayout(new BorderLayout(3, 3));
        cardPanel = new JPanel(layout = new CardLayout());
        add(cardPanel);
        objectDisplayLabel = new JLabel();
        JPanel headerPanel = new JPanel(new BorderLayout(7, 7));
        headerPanel.add(objectDisplayLabel, BorderLayout.WEST);
        add(headerPanel, BorderLayout.NORTH);
        showUsageCheckBox = new JCheckBox(new AbstractAction("Show usage") {

            /**
             * 
             */
            private static final long serialVersionUID = -8105770102874054033L;

            public void actionPerformed(ActionEvent e) {
                showUsagePanel(showUsageCheckBox.isSelected());
            }
        });
        headerPanel.add(showUsageCheckBox, BorderLayout.EAST);

        entityFrame = new OWLEntityFrame(getOWLEditorKit());
        List<OWLFrameSection> sections=entityFrame.getFrameSections();
        
        /* ******************* */
        System.out.println("section length: "+sections.size());
        for (OWLFrameSection section: sections){
        	
        	System.out.println("section name: "+section.getName()+
        			" root: "+section.getRootObject().toString());
        }
        
        /* ******************** */
        
        frameList = new OWLFrameList(getOWLEditorKit(), entityFrame);
        
        
        /* ***************** */
        System.out.println("num components "+frameList.getComponentCount());
        Component[] jcomponents = frameList.getComponents();
        System.out.println("inner listen: "+jcomponents[0].getKeyListeners().length);
        ComponentListener[] cListeners = frameList.getComponentListeners();
        System.out.println("comp num: "+jcomponents.length+" listeners: "+
        cListeners.length);
        List<OWLObject> objects = frameList.getObjectsToCopy();
        
        System.out.println("objects num: "+objects.size());
        for (int i=0;i < objects.size();i++){
        	System.out.println("object is: "+objects.get(i).toString());
        }
        /* ***************** */
        
        
        cardPanel.add("ENTITIES", new JScrollPane(frameList));
        owlEditorKit.getWorkspace().getOWLSelectionModel().addListener(new OWLSelectionModelListener() {
			@Override
			public void selectionChanged() throws Exception {
			    OWLObject selObj = getOWLWorkspace().getOWLSelectionModel().getSelectedObject();
			    frameList.setRootObject(selObj);
			    
			    List<OWLFrameSection> sections=entityFrame.getFrameSections();
		        
		        /* ******************* */
		        System.out.println("section length: "+sections.size());
		        for (OWLFrameSection section: sections){
		        	
		        	System.out.println("section name: "+section.getName()+
		        			" root: "+section.getRootObject().toString());
		        	List axioms = section.getAxioms();
		        	
		        	for (int i=0;i<axioms.size();i++){
		        		System.out.println("axiom: "+((OWLAxiom)axioms.get(i)).toString());
		        	}
		        }
		        
			    if (selObj instanceof OWLEntity) {
			        layout.show(cardPanel, "ENTITIES");
			    }
			    else if(selObj instanceof OWLOntology) {
			        ontologyPanel.setOntology((OWLOntology) selObj);
			        layout.show(cardPanel, "ONTOLOGY");
			    }
			    updateLabel(selObj);
			}
		});
        frameList.setRootObject(null);
        
        
        getOWLModelManager().addListener(new OWLModelManagerListener() {
			@Override
			public void handleChange(OWLModelManagerChangeEvent event) {
			    if(event.isType(EventType.ACTIVE_ONTOLOGY_CHANGED)) {
			        OWLOntology ontology = SelectedObjectPanelTest.this.getOWLModelManager().getActiveOntology();
			        ontologyPanel.setOntology(ontology);
			        layout.show(cardPanel, "ONTOLOGY");
			        updateLabel(ontology);
			    }
			}
		});
        cardPanel.add("ONTOLOGY", ontologyPanel = new OWLOntologyDisplayPanel(owlEditorKit));
        setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));

        usagePanel = new UsagePanel(owlEditorKit);
        usageSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        usageSplitPane.setResizeWeight(0.9);
        usageSplitPane.setRightComponent(usagePanel);
        usageSplitPane.setBorder(null);
    }

    private void showUsagePanel(boolean b) {
        if (b) {
            remove(cardPanel);
            usageSplitPane.setLeftComponent(cardPanel);
            add(usageSplitPane);
            revalidate();
        }
        else {
            remove(usageSplitPane);
            add(cardPanel);
            revalidate();
        }
    }

    private void updateLabel(final OWLObject object) {
        object.accept(new OWLObjectVisitorAdapter() {

            public void visit(OWLClass owlClass) {
                objectDisplayLabel.setText("<html><body><b>Class: </b>" + owlEditorKit.getModelManager().getRendering(object) + "</body></html>");
            }


            public void visit(OWLDataProperty owlDataProperty) {
                objectDisplayLabel.setText("<html><body><b>Data property: </b>" + owlEditorKit.getModelManager().getRendering(object) + "</body></html>");
            }


            public void visit(OWLIndividual owlIndividual) {
                objectDisplayLabel.setText("<html><body><b>Individual: </b>" + owlEditorKit.getModelManager().getRendering(object) + "</body></html>");
            }


            public void visit(OWLObjectProperty owlObjectProperty) {
                objectDisplayLabel.setText("<html><body><b>Object property: </b>" + owlEditorKit.getModelManager().getRendering(object) + "</body></html>");
            }


            public void visit(OWLOntology owlOntology) {
                objectDisplayLabel.setText("<html><body><b>Ontology: </b>" + owlEditorKit.getModelManager().getRendering(owlOntology) + "</body></html>");
            }
        });
    }

    public OWLEditorKit getOWLEditorKit() {
        return owlEditorKit;
    }

    public OWLWorkspace getOWLWorkspace() {
        return getOWLEditorKit().getWorkspace();
    }

    public OWLModelManager getOWLModelManager() {
        return getOWLEditorKit().getModelManager();
    }
}

class FrameTest extends OWLEntityFrame{
	
	private List<OWLFrameSection> owlClassSections;

    private List<OWLFrameSection> owlObjectPropertySections;
    
	public FrameTest(OWLEditorKit editor){
		super(editor);
		owlClassSections=super.getFrameSections();
	}

	public List<OWLFrameSection> getOwlClassSections() {
		return owlClassSections;
	}

	public void setOwlClassSections(List<OWLFrameSection> owlClassSections) {
		this.owlClassSections = owlClassSections;
	}

	public List<OWLFrameSection> getOwlObjectPropertySections() {
		return owlObjectPropertySections;
	}

	public void setOwlObjectPropertySections(List<OWLFrameSection> owlObjectPropertySections) {
		this.owlObjectPropertySections = owlObjectPropertySections;
	}
	
}

